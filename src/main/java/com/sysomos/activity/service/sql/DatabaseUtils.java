package com.sysomos.activity.service.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

public class DatabaseUtils {

	private static final Logger LOG = LoggerFactory
			.getLogger(DatabaseUtils.class);

	public DatabaseUtils() {
	}

	public <T> T executeReadOnlySQLQuery(ISQLQuery<T> query) {
		if (query == null)
			return null;
		PreparedStatement statement = query.prepareStatement();
		if (statement == null)
			return null;
		List<Map<String, SQLValue>> allRows = new ArrayList<Map<String, SQLValue>>();
		try {
			ResultSet results = statement.executeQuery();
			ResultSetMetaData md = results.getMetaData();
			int columns = md.getColumnCount();
			while (results.next()) {
				Map<String, SQLValue> row = Maps
						.newHashMapWithExpectedSize(columns);
				for (int i = 1; i <= columns; i++) {
					SQLValue value = new SQLValue(results.getObject(i),
							md.getColumnType(i));
					row.put(md.getColumnName(i), value);
				}
				allRows.add(row);
			}
			closeOrLogError(results);
			return query.parseQueryResults(allRows);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage(), e);
		} finally {
			closeOrLogError(statement);
		}
		return null;
	}

	private void closeOrLogError(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				String message = "Closing SQL results set failed [ "
						+ e.getLocalizedMessage() + " ]";
				LOG.error(message, e);
			}
		}

	}

	void closeOrLogError(PreparedStatement stmt) {
		if (stmt != null) {
			try {
				stmt.getConnection().close();
				stmt.close();
			} catch (SQLException e) {
				String message = "Closing SQL Statement failed [ "
						+ e.getLocalizedMessage() + " ]";
				LOG.error(message, e);
			}
		}
	}
}
