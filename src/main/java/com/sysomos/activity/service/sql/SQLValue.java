package com.sysomos.activity.service.sql;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Types;
import java.util.Date;

/**
 * @see "http://www.tutorialspoint.com/jdbc/jdbc-data-types.htm"
 * @author adia
 *
 */
public class SQLValue {

	private Object columnValue;
	private int columnType;

	public SQLValue(Object columnValue, int columnType) {
		this.columnValue = columnValue;
		this.columnType = columnType;
	}

	public Integer getInteger() {
		return (columnType == Types.INTEGER) ? (Integer) columnValue : null;
	}

	public BigInteger getBigInteger() {
		return (columnType == Types.BIGINT) ? (BigInteger) columnValue : null;
	}

	public Short getShort() {
		return (columnType == Types.SMALLINT) ? (Short) columnValue : null;
	}

	public Byte getByte() {
		return (columnType == Types.TINYINT) ? (Byte) columnValue : null;
	}

	public String getString() {
		return (columnType == Types.VARCHAR || columnType == Types.NCHAR
				|| columnType == Types.LONGNVARCHAR || columnType == Types.CLOB
				|| columnType == Types.BLOB || columnType == Types.CLOB) ? (String) columnValue
				: null;
	}

	public Double getDouble() {
		return (columnType == Types.DOUBLE) ? (Double) columnValue : null;
	}

	public Long getLong() {
		return (columnType == Types.BIGINT) ? (Long) columnValue : null;
	}

	public BigDecimal getDecimal() {
		return (columnType == Types.NUMERIC) ? (BigDecimal) columnValue : null;
	}

	public Float getFloat() {
		return (columnType == Types.REAL || columnType == Types.FLOAT) ? (Float) columnValue
				: null;
	}

	public Date getDate() {
		return (columnType == Types.DATE) ? (Date) columnValue : null;
	}
}
