package com.sysomos.activity.service.sql;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.ActivityService.ActivityType;
import com.sysomos.activity.service.UserToUserActivity;
import com.sysomos.activity.service.utils.Pair;

public class LookupUserToUserActivityQuery extends BasicSQLStatement implements
		ISQLQuery<UserToUserActivity> {

	private static final String SQLQ1 = "select userid, original_id, count(*) nb from twitter_user_activity "
			+ "where userid = ? and original_id = ? group by userid, original_id";

	private static final String SQLQ2 = "select userid, original_id, activity_type, count(*) nb from "
			+ "twitter_user_activity where userid = ? and original_id = ? and activity_type = ? "
			+ "group by userid, original_id, activity_type";

	private static final String SQLQ3 = "select userid, original_id, count(*) nb from twitter_user_activity "
			+ "where userid = ? and original_id = ? and time >= ? and time <= ? group by userid, original_id";

	private static final String SQLQ4 = "select userid, original_id, activity_type, count(*) nb from "
			+ "twitter_user_activity where userid = ? and original_id = ? and activity_type = ? and "
			+ "time >= ? and time <= ? group by userid, original_id, activity_type";

	private static final Logger LOG = LoggerFactory
			.getLogger(LookupUserToUserActivityQuery.class);

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 */
	public LookupUserToUserActivityQuery(long id1, long id2) {
		super(SQLQ1, id1, id2);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a specific (non-null) type of activity.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param activityType
	 *            Activity type descriptor (@see
	 *            {@link ActivityService.ActivityType}). Should not be null !
	 */
	public LookupUserToUserActivityQuery(long id1, long id2, int activityType) {
		super(SQLQ2, id1, id2, activityType);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a given time window.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupUserToUserActivityQuery(long id1, long id2,
			long startDateMillis, long endDateMillis) {
		super(SQLQ3, id1, id2, startDateMillis, endDateMillis);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a specific (non-null) type of activity on
	 * a given time window.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType}).
	 *            Should not be null !
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupUserToUserActivityQuery(long id1, long id2, int activityType,
			long startDateMillis, long endDateMillis) {
		super(SQLQ4, id1, id2, activityType, startDateMillis, endDateMillis);
	}

	@Override
	public UserToUserActivity parseQueryResults(
			final List<Map<String, SQLValue>> results) {
		if (CollectionUtils.isEmpty(results))
			return null;
		if (results.size() > 1) {
			LOG.error("Retrieved multiple rows when one at most was expected. "
					+ "Defaulting to first row");
		}
		Map<String, SQLValue> firstRow = results.get(0);
		Pair<Long, Long> pair = new Pair<Long, Long>(firstRow.get("userid")
				.getBigInteger().longValue(), firstRow.get("original_id")
				.getBigInteger().longValue());
		ActivityType type = null;

		if (firstRow.get("activity_type") != null) {
			type = ActivityType.get(firstRow.get("activity_type").getInteger());
		}
		int engagement = firstRow.get("nb").getLong().intValue();
		return new UserToUserActivity(pair, type, engagement);
	}

}
