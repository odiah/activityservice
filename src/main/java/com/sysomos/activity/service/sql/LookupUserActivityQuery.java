package com.sysomos.activity.service.sql;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.utils.Pair;

public class LookupUserActivityQuery extends BasicSQLStatement implements
		ISQLQuery<Pair<Long, Integer>> {

	private static final String SQLQ1 = "select userid, count(userid) nb from twitter_user_activity "
			+ "where userid = ? group by userid";

	private static final String SQLQ2 = "select userid, count(userid) nb from twitter_user_activity "
			+ "where userid = ? and activity_type = ? group by userid";

	private static final String SQLQ3 = "select userid, count(userid) nb from twitter_user_activity "
			+ "where userid = ? and activity_type = ? and time >= ? and time <= ? group by userid";

	private static final String SQLQ4 = "select userid, count(userid) nb from twitter_user_activity "
			+ "where userid = ? and time >= ? and time <= ? group by userid";

	private static final Logger LOG = LoggerFactory
			.getLogger(LookupUserActivityQuery.class);

	/**
	 * Call this constructor when querying the activity of a user with respect
	 * to other users' posts.
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 */
	public LookupUserActivityQuery(long userId) {
		super(SQLQ1, userId);
	}

	/**
	 * Call this constructor when querying a specific type of activity of a user
	 * with respect to other users' posts.
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @param activityType
	 *            Activity type descriptor (@see
	 *            {@link ActivityService.ActivityType}). Should not be null User
	 *            whose posts userId1 is engaging in.
	 */
	public LookupUserActivityQuery(long userId, final Integer activityType) {
		super(SQLQ2, userId, activityType);
	}

	/**
	 * Call this constructor when querying a specific type of activity of a user
	 * with respect to other users' posts for a given time window.
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @param type
	 *            Activity type descriptor (@see
	 *            {@link ActivityService.ActivityType}). Should not be null User
	 *            whose posts userId1 is engaging in.
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupUserActivityQuery(long userId, final Integer activityType,
			long startTimeMillis, long endTimeMillis) {
		super(SQLQ3, userId, activityType, startTimeMillis, endTimeMillis);
	}

	/**
	 * Call this constructor when querying the activity of a user with respect
	 * to other users' posts for a given time window.
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupUserActivityQuery(long userId, long startTimeMillis,
			long endTimeMillis) {
		super(SQLQ4, userId, startTimeMillis, endTimeMillis);
	}

	@Override
	public Pair<Long, Integer> parseQueryResults(
			final List<Map<String, SQLValue>> results) {
		if (CollectionUtils.isEmpty(results))
			return null;
		if (results.size() > 1) {
			LOG.error("Retrieved multiple rows when one at most was expected. "
					+ "Defaulting to first row");
		}
		Map<String, SQLValue> firstRow = results.get(0);

		Pair<Long, Integer> pair = new Pair<Long, Integer>(firstRow
				.get("userid").getBigInteger().longValue(), firstRow.get("nb")
				.getLong().intValue());
		return pair;
	}

}
