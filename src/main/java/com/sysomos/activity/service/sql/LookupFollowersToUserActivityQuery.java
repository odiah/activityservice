package com.sysomos.activity.service.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.ActivityService.ActivityType;
import com.sysomos.activity.service.UserToUserActivity;
import com.sysomos.activity.service.utils.Pair;

public class LookupFollowersToUserActivityQuery extends BasicSQLStatement
		implements ISQLQuery<List<UserToUserActivity>> {

	private static final String SQLQ1 = "select userid, original_id, count(*) nb from twitter_user_activity "
			+ "where original_id = ? group by userid, original_id";

	private static final String SQLQ2 = "select userid, original_id, activity_type, count(*) nb from "
			+ "twitter_user_activity where original_id = ? and activity_type = ? "
			+ "group by userid, original_id, activity_type";

	private static final String SQLQ3 = "select userid, original_id, count(*) nb from twitter_user_activity "
			+ "where original_id = ? and time >= ? and time <= ? group by userid, original_id";

	private static final String SQLQ4 = "select userid, original_id, activity_type, count(*) nb from "
			+ "twitter_user_activity where original_id = ? and activity_type = ? and "
			+ "time >= ? and time <= ? group by userid, original_id, activity_type";

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 */
	public LookupFollowersToUserActivityQuery(long id) {
		super(SQLQ1, id);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a specific (non-null) type of activity.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param activityType
	 *            Activity type descriptor (@see
	 *            {@link ActivityService.ActivityType}). Should not be null !
	 */
	public LookupFollowersToUserActivityQuery(long id, int activityType) {
		super(SQLQ2, id, activityType);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a given time window.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupFollowersToUserActivityQuery(long id, long startDateMillis,
			long endDateMillis) {
		super(SQLQ3, id, startDateMillis, endDateMillis);
	}

	/**
	 * Call this constructor when querying the activity of two users with
	 * respect to each other posts for a specific (non-null) type of activity on
	 * a given time window.
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType}).
	 *            Should not be null !
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 */
	public LookupFollowersToUserActivityQuery(long id, int activityType,
			long startDateMillis, long endDateMillis) {
		super(SQLQ4, id, activityType, startDateMillis, endDateMillis);
	}

	@Override
	public List<UserToUserActivity> parseQueryResults(
			final List<Map<String, SQLValue>> results) {
		if (CollectionUtils.isEmpty(results))
			return null;
		List<UserToUserActivity> activities = new ArrayList<UserToUserActivity>();
		for (Map<String, SQLValue> tuple : results) {
			Pair<Long, Long> pair = new Pair<Long, Long>(tuple.get("userid")
					.getBigInteger().longValue(), tuple.get("original_id")
					.getBigInteger().longValue());
			ActivityType type = null;

			if (tuple.get("activity_type") != null) {
				type = ActivityType
						.get(tuple.get("activity_type").getInteger());
			}
			int engagement = tuple.get("nb").getLong().intValue();
			activities.add(new UserToUserActivity(pair, type, engagement));
		}
		return activities;
	}

}
