package com.sysomos.activity.service.exception;

public class ActivityServiceException extends Exception {

	private static final long serialVersionUID = -6611428769319094677L;

	public ActivityServiceException(String message) {
		super(message);
	}

	public ActivityServiceException(Throwable cause) {
		super(cause);
	}

	public ActivityServiceException(Exception e) {
		super(e);
	}
}
