package com.sysomos.activity.service.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	public static List<Long> newList(long userId1, long userId2) {
		List<Long> ids = new ArrayList<Long>();
		ids.add(userId1);
		ids.add(userId2);
		return ids;
	}
}
