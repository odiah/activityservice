package com.sysomos.activity.service.utils;

/**
 * A simple tuple <a,b> with two objects in a pair.
 */
public class Pair<TypeA, TypeB> implements Comparable<Pair<TypeA, TypeB>> {
	protected final TypeA a;

	protected final TypeB b;

	public Pair(TypeA key, TypeB value) {
		a = key;
		b = value;
	}

	@Override
	public int compareTo(Pair<TypeA, TypeB> p1) {
		if (null != p1) {
			if (p1.equals(this)) {
				return 0;
			} else if (p1.hashCode() > this.hashCode()) {
				return 1;
			} else if (p1.hashCode() < this.hashCode()) {
				return -1;
			}
		}
		return (-1);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Pair) {
			Pair<?, ?> p1 = (Pair<?, ?>) o;
			if (p1.a.equals(this.a) && p1.b.equals(this.b)) {
				return (true);
			}
		}
		return (false);
	}

	public TypeA getA() {
		return a;
	}

	public TypeB getB() {
		return b;
	}

	@Override
	public int hashCode() {
		int hashCode = a.hashCode() + (31 * b.hashCode());
		return (hashCode);
	}

	public String toString() {
		StringBuffer buff = new StringBuffer();
		buff.append("Key: ");
		buff.append(a);
		buff.append("\tValue: ");
		buff.append(b);
		return (buff.toString());
	}
}
