package com.sysomos.activity.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.sysomos.activity.service.exception.ActivityServiceException;

public class GetUserToUserActivity {

	private static final Logger LOG = LoggerFactory
			.getLogger(GetUserToUserActivity.class);

	private static Set<Long> userIds = new HashSet<Long>();

	public static class Request {

		private Batch batch;

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for all types of activities. This is
		 * not time bounded.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 */
		public Request(final List<Long> userIds) {
			this(userIds, null, null);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for the specified activity. This is
		 * not time bounded.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public Request(final List<Long> userIds, final Integer activityType) {
			this(userIds, activityType, null, null);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for all types of activities.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public Request(final List<Long> userIds, Long startDateMillis,
				Long endDateMillis) {
			this(userIds, null, startDateMillis, endDateMillis);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for the specified activity.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public Request(final List<Long> userIds, final Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			batch = new Batch(2);
			batch.addRequest(userIds, activityType);
		}

		public ResultContext call() {
			return batch.call();
		}
	}

	public static class Batch {
		private GetFollowersToUserActivity.Batch folBatch;
		private GetUserToFriendsActivity.Batch frdBatch;

		public Batch(int batchSize) {
			folBatch = new GetFollowersToUserActivity.Batch(batchSize);
			frdBatch = new GetUserToFriendsActivity.Batch(batchSize);
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param userId
		 */
		public void addRequest(long userId) {
			addRequest(userId, null);
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param userId
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(long userId, Integer activityType) {
			addRequest(userId, activityType, null, null);
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param userId
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(long userId, Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			GetUserToUserActivity.userIds.add(userId);
			frdBatch.addRequest(userId, activityType, startDateMillis,
					endDateMillis);
			folBatch.addRequest(userId, activityType, startDateMillis,
					endDateMillis);
		}

		/**
		 * Create a collection of new requests and add them to the queue
		 * 
		 * @param userIds
		 */
		public void addRequest(List<Long> userIds) {
			addRequest(userIds, null);
		}

		/**
		 * Create a collection of new requests and add them to the queue
		 * 
		 * @param userIds
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(List<Long> userIds, Integer activityType) {
			addRequest(userIds, activityType, null, null);
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 * @param activityType
		 *            activity type (@see {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(final List<Long> userIds, Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			if (CollectionUtils.isEmpty(userIds))
				return;
			for (long id : userIds) {	
				addRequest(id, activityType, startDateMillis, endDateMillis);
			}
		}

		public ResultContext call() {
			ResultContext rsltContext = new ResultContext();
			GetUserToFriendsActivity.ResultContext frdCtxt = frdBatch.call();
			System.out.println("*** RsltContext (Friends) Before Filtering: "
					+ frdCtxt.size());
			for (int i = 0; i < frdCtxt.size(); i++) {
				try {
					populate(rsltContext, filter(frdCtxt.get(i)));
				} catch (ActivityServiceException e) {
					LOG.error(e.getLocalizedMessage(), e);
				}
			}
			GetFollowersToUserActivity.ResultContext folCtxt = folBatch.call();
			System.out.println("*** RsltContext (Followers) Before Filtering: "
					+ folCtxt.size());
			for (int i = 0; i < folCtxt.size(); i++) {
				try {
					populate(rsltContext, filter(folCtxt.get(i)));
				} catch (ActivityServiceException e) {
					LOG.error(e.getLocalizedMessage(), e);
				}
			}
			return rsltContext;
		}

		private void populate(ResultContext rsltContext,
				Collection<UserToUserActivity> results) {
			if (!CollectionUtils.isEmpty(results)) {
				rsltContext.add(results);
			}
		}

		private Set<UserToUserActivity> filter(List<UserToUserActivity> activity) {
			if (CollectionUtils.isEmpty(activity)) {
				return null;
			}
			Set<UserToUserActivity> results = new HashSet<UserToUserActivity>();
			for (UserToUserActivity instance : activity) {
				if (instance == null)
					continue;
				long userI = instance.getIdPair().getA();
				long userJ = instance.getIdPair().getB();
				if (!GetUserToUserActivity.userIds.contains(userI)
						|| !GetUserToUserActivity.userIds.contains(userJ))
					continue;
				results.add(instance);
			}
			return results;
		}
	}

	public static class ResultContext {
		private List<UserToUserActivity> results;

		public ResultContext() {
			results = new ArrayList<UserToUserActivity>();
		}

		public void add(final Collection<UserToUserActivity> activities) {
			if (!CollectionUtils.isEmpty(activities))
				results.addAll(activities);
		}

		public void add(final UserToUserActivity userToUserActivity) {
			results.add(userToUserActivity);
		}

		public UserToUserActivity get(int index)
				throws ActivityServiceException {
			if (index >= results.size())
				throw new ActivityServiceException("Index out of bounds");
			return results.get(index);
		}

		public int size() {
			return results.size();
		}
	}
}
