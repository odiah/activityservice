package com.sysomos.activity.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.sysomos.activity.service.ActivityService.ActivityType;
import com.sysomos.activity.service.exception.ActivityServiceException;
import com.sysomos.activity.service.sql.DatabaseUtils;
import com.sysomos.activity.service.sql.LookupUserToUserActivityQuery;
import com.sysomos.activity.service.utils.Pair;

public class GetUserToUserActivityOld {

	private static final Logger LOG = LoggerFactory
			.getLogger(GetUserToUserActivityOld.class);

	/**
	 * 
	 * @author adia
	 *
	 */
	public static class Request {

		private final Pair<Long, Long> pair;

		private Integer activityType = null;
		private final Long startDateMillis;
		private final Long endDateMillis;

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for all types of activities. This is
		 * not time bounded.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 */
		public Request(final Pair<Long, Long> pair) {
			this(pair, null, null);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for the specified activity. This is
		 * not time bounded.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public Request(final Pair<Long, Long> pair, final Integer activityType) {
			this(pair, activityType, null, null);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for all types of activities.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public Request(final Pair<Long, Long> pair, Long startDateMillis,
				Long endDateMillis) {
			this(pair, null, startDateMillis, endDateMillis);
		}

		/**
		 * When this constructor is called, we look at the activity of both
		 * users with respect to each other for the specified activity.
		 * 
		 * @param ids
		 *            pair of user IDs.
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public Request(final Pair<Long, Long> pair, final Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			this.pair = pair;
			this.activityType = activityType;
			this.startDateMillis = startDateMillis;
			this.endDateMillis = endDateMillis;
		}

		public ResultContext call() {
			List<Request> requests = new ArrayList<Request>();
			requests.add(this);
			ResultContext rsltContext = new ResultContext();
			rsltContext.add(executeQueries(requests));
			return rsltContext;
		}
	}

	/**
	 * 
	 * @author adia
	 *
	 */
	public static class ResultContext {
		private List<UserToUserActivity> results;

		public ResultContext() {
			results = new ArrayList<UserToUserActivity>();
		}

		public void add(final List<UserToUserActivity> activities) {
			results.addAll(activities);
		}

		public void add(final UserToUserActivity userToUserActivity) {
			results.add(userToUserActivity);
		}

		public UserToUserActivity get(int index)
				throws ActivityServiceException {
			if (index >= results.size())
				throw new ActivityServiceException("Index out of bounds");
			return results.get(index);
		}

		public int size() {
			return results.size();
		}
	}

	/**
	 * 
	 * @author adia
	 *
	 */
	public static class Batch {

		private int batchSize;
		private final AtomicInteger countInBatch;
		private ConcurrentLinkedQueue<Request> queue;

		public Batch(int batchSize) {
			this.batchSize = batchSize;
			this.countInBatch = new AtomicInteger(0);
			queue = new ConcurrentLinkedQueue<Request>();
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param pair
		 */
		public void addRequest(Pair<Long, Long> pair) {
			queue.add(new Request(pair));
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param pair
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(Pair<Long, Long> pair, Integer activityType) {
			queue.add(new Request(pair, activityType));
		}

		/**
		 * Create a collection of new requests and add them to the queue
		 * 
		 * @param pairs
		 */
		public void addRequest(List<Pair<Long, Long>> pairs) {
			addRequest(pairs, null);
		}

		/**
		 * Create a collection of new requests and add them to the queue
		 * 
		 * @param pairs
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(List<Pair<Long, Long>> pairs,
				Integer activityType) {
			if (CollectionUtils.isEmpty(pairs))
				return;
			for (Pair<Long, Long> pair : pairs) {
				addRequest(pair, activityType);
			}
		}

		/**
		 * 
		 * @param pair
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(Pair<Long, Long> pair, Integer activityType,
				long startDateMillis, long endDateMillis) {
			queue.add(new Request(pair, activityType, startDateMillis,
					endDateMillis));
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 * @param types
		 *            List of activity types (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(final List<Long> userIds,
				final List<ActivityType> types) {
			addRequest(userIds, types, null, null);
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 * @param types
		 *            List of activity types (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(final List<Long> userIds,
				final List<ActivityType> types, Long startDateMillis,
				Long endDateMillis) {
			if (CollectionUtils.isEmpty(userIds))
				return;
			List<Pair<Long, Long>> pairList = getPairs(userIds);
			if (CollectionUtils.isEmpty(types)) {
				for (Pair<Long, Long> pair : pairList) {
					queue.add(new Request(pair, startDateMillis, endDateMillis));
				}
			} else {
				for (Pair<Long, Long> pair : pairList) {
					for (ActivityType activity : types) {
						if (activity == null)
							continue;
						queue.add(new Request(pair, activity.descriptor,
								startDateMillis, endDateMillis));
					}
				}
			}
		}

		private List<UserToUserActivity> batchProcess() {
			int size, old;
			List<UserToUserActivity> results = new ArrayList<UserToUserActivity>();
			while (!queue.isEmpty()) {
				if (queue.size() < batchSize) {
					List<Request> requests = createBatch(queue.size());
					results.addAll(executeQueries(requests));
					break;
				}
				do {
					old = countInBatch.get();
					size = (old + 1) % batchSize;
				} while (!countInBatch.compareAndSet(old, size));
				if (size == 0) {
					List<Request> requests = createBatch(batchSize);
					results.addAll(executeQueries(requests));
					if (!queue.isEmpty()) {
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							LOG.error(e.getLocalizedMessage(), e);
						}
					}
				}
			}
			return results;
		}

		public ResultContext call() {
			ResultContext rslCtx = new ResultContext();
			rslCtx.add(batchProcess());
			return rslCtx;
		}

		private List<Request> createBatch(int size) {
			List<Request> batch = Lists.newArrayListWithCapacity(size);
			for (int i = 0; i < size; i++) {
				Request entry = queue.remove();
				if (entry == null) {
					LOG.warn("Queue has fewer elements than expected: "
							+ batch.size() + " instead of " + size);
					break;
				}
				batch.add(entry);
			}
			return batch;
		}

		public int size() {
			return queue.size();
		}
	}

	private static List<Pair<Long, Long>> getPairs(List<Long> userIds) {
		if (CollectionUtils.isEmpty(userIds))
			return null;
		List<Pair<Long, Long>> pairs = new ArrayList<Pair<Long, Long>>();
		for (int i = 0; i < userIds.size(); i++) {
			long userI = userIds.get(i);
			for (int j = i + 1; j < userIds.size(); j++) {
				long userJ = userIds.get(j);
				pairs.add(new Pair<Long, Long>(userI, userJ));
				pairs.add(new Pair<Long, Long>(userJ, userI));
			}
		}
		return pairs;
	}

	/**
	 * 
	 * @param requests
	 * @return
	 */
	private static List<UserToUserActivity> executeQueries(
			final List<Request> requests) {
		List<UserToUserActivity> lookupRslts;
		lookupRslts = new ArrayList<UserToUserActivity>();
		if (CollectionUtils.isEmpty(requests))
			return null;

		LookupUserToUserActivityQuery query = null;
		DatabaseUtils dbUtils = new DatabaseUtils();
		for (Request req : requests) {
			if (req.startDateMillis != null && req.endDateMillis != null
					&& req.activityType != null) {
				query = new LookupUserToUserActivityQuery(req.pair.getA(),
						req.pair.getB(), req.activityType, req.startDateMillis,
						req.endDateMillis);
			} else if (req.activityType != null) {
				query = new LookupUserToUserActivityQuery(req.pair.getA(),
						req.pair.getB(), req.activityType);
			} else if (req.startDateMillis != null && req.endDateMillis != null) {
				query = new LookupUserToUserActivityQuery(req.pair.getA(),
						req.pair.getB(), req.startDateMillis, req.endDateMillis);
			} else {
				query = new LookupUserToUserActivityQuery(req.pair.getA(),
						req.pair.getB());
			}
			lookupRslts.add(dbUtils.executeReadOnlySQLQuery(query));
		}
		return lookupRslts;
	}

}
