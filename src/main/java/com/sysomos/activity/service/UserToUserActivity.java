package com.sysomos.activity.service;

import com.sysomos.activity.service.ActivityService.ActivityType;
import com.sysomos.activity.service.utils.Pair;

public class UserToUserActivity {

	private int activity;
	private ActivityType type = null;
	private final Pair<Long, Long> idPair;

	public UserToUserActivity(final Pair<Long, Long> idPair,
			final ActivityType type, int activity) {
		this.idPair = idPair;
		this.type = type;
		this.activity = activity;
	}

	public String getType() {
		if (type == null)
			return "ALL";
		return type.name();
	}

	public Pair<Long, Long> getIdPair() {
		return idPair;
	}

	public int getEngagement() {
		return activity;
	}

	public String toString() {
		String toReturn = "User I: " + idPair.getA() + ", User II: "
				+ idPair.getB() + ", Type: " + getType() + ", Activity: "
				+ activity;
		return toReturn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + activity;
		result = prime * result + ((idPair == null) ? 0 : idPair.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserToUserActivity other = (UserToUserActivity) obj;
		if (activity != other.activity)
			return false;
		if (idPair == null) {
			if (other.idPair != null)
				return false;
		} else if (!idPair.equals(other.idPair))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	
}
