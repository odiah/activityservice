package com.sysomos.activity.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.sysomos.activity.service.exception.ActivityServiceException;
import com.sysomos.activity.service.sql.DatabaseUtils;
import com.sysomos.activity.service.sql.LookupUserToFriendsActivityQuery;

public class GetUserToFriendsActivity {

	private static final Logger LOG = LoggerFactory
			.getLogger(GetFollowersToUserActivity.class);

	public static class Request {

		private final long userId;
		private Integer activityType = null;
		private final Long startDateMillis;
		private final Long endDateMillis;

		public Request(long userId) {
			this(userId, null);
		}

		public Request(long userId, Integer activityType) {
			this(userId, activityType, null, null);
		}

		public Request(long userId, final Integer activityType,
				final Long startDateMillis, final Long endDateMillis) {
			this.activityType = activityType;
			this.userId = userId;
			this.startDateMillis = startDateMillis;
			this.endDateMillis = endDateMillis;
		}

		public ResultContext call() throws ActivityServiceException {
			List<Request> requests = new ArrayList<Request>();
			requests.add(this);
			ResultContext rsltContext = new ResultContext();
			rsltContext.add(executeQueries(requests));
			return rsltContext;
		}

		public Request(long userId, Long startDateMillis, Long endDateMillis) {
			this(userId, null, startDateMillis, endDateMillis);
		}
	}

	public static class Batch {

		private int batchSize;
		private final AtomicInteger countInBatch;
		private ConcurrentLinkedQueue<Request> queue;

		public Batch(int batchSize) {
			this.batchSize = batchSize;
			this.countInBatch = new AtomicInteger(0);
			queue = new ConcurrentLinkedQueue<Request>();
		}

		public void addRequest(long userId) {
			addRequest(userId, null);
		}

		public void addRequest(long userId, Integer activityType) {
			addRequest(userId, activityType, null, null);
		}

		/**
		 * Create a new request and add it to the queue
		 * 
		 * @param userId
		 * @param activityType
		 *            Activity type descriptor (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(long userId, Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			queue.add(new Request(userId, activityType, startDateMillis,
					endDateMillis));
		}

		/**
		 * Create a collection of new requests and add them to the queue
		 * 
		 * @param userIds
		 */
		public void addRequest(final List<Long> userIds) {
			addRequest(userIds, null);
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 * @param types
		 *            List of activity types (@see
		 *            {@link ActivityService.ActivityType})
		 */
		public void addRequest(List<Long> userIds, Integer activityType) {
			addRequest(userIds, activityType, null, null);
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 * @param types
		 *            List of activity types (@see
		 *            {@link ActivityService.ActivityType})
		 * @param startDateMillis
		 *            The start date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 * @param endDateMillis
		 *            The end date in milliseconds for the time window to
		 *            consider when querying the user activity table.
		 */
		public void addRequest(final List<Long> userIds, Integer activityType,
				Long startDateMillis, Long endDateMillis) {
			if (CollectionUtils.isEmpty(userIds))
				return;
			for (long id : userIds) {
				addRequest(id, activityType, startDateMillis, endDateMillis);
			}
		}

		private List<List<UserToUserActivity>> batchProcess() {
			int size, old;
			List<List<UserToUserActivity>> results = new ArrayList<List<UserToUserActivity>>();
			while (!queue.isEmpty()) {
				if (queue.size() < batchSize) {
					List<Request> requests = createBatch(queue.size());
					results.addAll(executeQueries(requests));
					break;
				}
				do {
					old = countInBatch.get();
					size = (old + 1) % batchSize;
				} while (!countInBatch.compareAndSet(old, size));
				if (size == 0) {
					List<Request> requests = createBatch(batchSize);
					results.addAll(executeQueries(requests));
					if (!queue.isEmpty()) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							LOG.error(e.getLocalizedMessage(), e);
						}
					}
				}
			}
			return results;
		}

		public ResultContext call() {
			ResultContext rslCtx = new ResultContext();
			rslCtx.add(batchProcess());
			return rslCtx;
		}

		private List<Request> createBatch(int size) {
			List<Request> batch = Lists.newArrayListWithCapacity(size);
			for (int i = 0; i < size; i++) {
				Request entry = queue.remove();
				if (entry == null) {
					LOG.warn("Queue has fewer elements than expected: "
							+ batch.size() + " instead of " + size);
					break;
				}
				batch.add(entry);
			}
			return batch;
		}
	}

	private static List<List<UserToUserActivity>> executeQueries(
			final List<Request> requests) {
		if (CollectionUtils.isEmpty(requests))
			return null;
		List<List<UserToUserActivity>> lookupRslts;
		lookupRslts = new ArrayList<List<UserToUserActivity>>();
		LookupUserToFriendsActivityQuery query = null;
		DatabaseUtils dbUtils = new DatabaseUtils();
		for (Request req : requests) {
			if (!(req.startDateMillis == null || req.endDateMillis == null
					|| req.activityType == null)) {
				query = new LookupUserToFriendsActivityQuery(req.userId,
						req.activityType, req.startDateMillis,
						req.endDateMillis);
			} else if (!(req.startDateMillis == null
					|| req.endDateMillis == null)) {
				query = new LookupUserToFriendsActivityQuery(req.userId,
						req.startDateMillis, req.endDateMillis);
			} else if (req.activityType != null) {
				query = new LookupUserToFriendsActivityQuery(req.userId,
						req.activityType);
			} else {
				query = new LookupUserToFriendsActivityQuery(req.userId);
			}
			lookupRslts.add(dbUtils.executeReadOnlySQLQuery(query));
		}
		return lookupRslts;
	}

	public static class ResultContext {

		private List<List<UserToUserActivity>> results;

		public ResultContext() {
			results = new ArrayList<List<UserToUserActivity>>();
		}

		public void add(final List<List<UserToUserActivity>> rows) {
			results.addAll(rows);
		}

		public List<UserToUserActivity> get(int index)
				throws ActivityServiceException {
			if (index >= results.size())
				throw new ActivityServiceException("Index out of bounds");
			return results.get(index);
		}

		public int size() {
			return results.size();
		}
	}
}
