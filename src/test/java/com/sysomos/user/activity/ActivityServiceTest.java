package com.sysomos.user.activity;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.ActivityService.ActivityType;
import com.sysomos.activity.service.GetUserActivity;
import com.sysomos.activity.service.GetUserToUserActivity;
import com.sysomos.activity.service.UserToUserActivity;
import com.sysomos.activity.service.exception.ActivityServiceException;
import com.sysomos.activity.service.utils.Pair;

public class ActivityServiceTest {

	private static final List<Long> IDs = new ArrayList<Long>();
	private static final Logger LOG = LoggerFactory
			.getLogger(ActivityServiceTest.class);
	private static final long StartDateMillis, EndDateMillis;

	static {
		IDs.add(223186092L);
		IDs.add(89848070L);
		IDs.add(2901854536L);
		IDs.add(2485244719L);
		IDs.add(1287002088L);
		IDs.add(2981817760L);
		IDs.add(460360348L);
		IDs.add(1558141890L);
		IDs.add(595897800L);
		IDs.add(46115651L);
		IDs.add(741157423L);
		IDs.add(36052038L);
		IDs.add(341391044L);
		IDs.add(615312474L);
		IDs.add(980765005L);
		IDs.add(115889467L);
		IDs.add(2303440400L);
		IDs.add(286753424L);
		IDs.add(133959115L);
		IDs.add(624266711L);

		StartDateMillis = new DateTime().minusDays(7).getMillis();
		EndDateMillis = new DateTime().minusDays(1).getMillis();
	}

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {

	}

	@Test
	public void newGetUserActivityRequestTest() {
		try {
			GetUserActivity.ResultContext context = ActivityService
					.newGetUserActivityRequest(223186092L).call();
			System.out.println("*******************************");
			System.out.println("User: " + 223186092L);
			System.out.println("*******************************");
			prettyPrinting(context);

			context = ActivityService.newGetUserActivityRequest(223186092L,
					ActivityType.REPLY).call();
			System.out.println("*******************************");
			System.out.println("User: " + 223186092L + ", Type: "
					+ ActivityType.REPLY);
			System.out.println("*******************************");
			prettyPrinting(context);

			context = ActivityService.newGetUserActivityRequest(223186092L,
					ActivityType.RETWEET).call();
			System.out.println("*******************************");
			System.out.println("User: " + 223186092L + ", Type: "
					+ ActivityType.RETWEET);
			System.out.println("*******************************");
			prettyPrinting(context);

		} catch (ActivityServiceException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	private void prettyPrinting(GetUserActivity.ResultContext context) {
		assertTrue(context != null);
		try {
			int size = context.size();
			for (int i = 0; i < size; i++) {
				Pair<Long, Integer> pair = context.get(i);
				System.out.println("[ " + pair + " ]");
			}
		} catch (ActivityServiceException e) {

		}
	}

	private void prettyPrinting(GetUserToUserActivity.ResultContext context) {
		assertTrue(context != null);
		try {
			int size = context.size();
			for (int i = 0; i < size; i++) {
				UserToUserActivity activity = context.get(i);
				System.out.println("[ " + activity + " ]");
			}
		} catch (ActivityServiceException e) {

		}
	}

	@Test
	public void newGetUserActivityBatchTest() {
		GetUserActivity.Batch batch = ActivityService.newGetUserActivityBatch();
		for (long userId : IDs) {
			batch.addRequest(userId);
		}
		prettyPrinting(batch.call());
	}

	@Test
	public void newGetUserToUserActivityRequestTest() {
		GetUserToUserActivity.ResultContext context = ActivityService
				.newGetUserToUserActivityRequest(223186092L, 89848070L).call();
		prettyPrinting(context);
	}

	@Test
	public void newGetUserToUserActivityBatchTest() {
		GetUserToUserActivity.Batch batch = ActivityService
				.newGetUserToUserActivityBatch();
		batch.addRequest(IDs);
		prettyPrinting(batch.call());
	}

	@Test
	public void newGetUserToUserActivityBatchTest2() {
		GetUserToUserActivity.Batch batch = ActivityService
				.newGetUserToUserActivityBatch(15);
		batch.addRequest(IDs, null, StartDateMillis, EndDateMillis);
		prettyPrinting(batch.call());
	}

	@After
	public void tearDown() {

	}
}
